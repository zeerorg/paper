var Web3 = require('web3');
var MyEthClass = new require('ethlib').Class;
var fs = require('fs');
var solc = require('solc');
var assert = require('assert');
var express = require('express');

var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
var ethLib = new MyEthClass(web3);

let contractOutputCache = null;

async function parseContract() {
  let output;
  if (!contractOutputCache) {
    const input = fs.readFileSync('../contract/policy_contract.sol');
    output = solc.compile(input.toString(), 1);
    contractOutputCache = output;
  } else {
    output = contractOutputCache;
  }
  // console.log(output.contracts)
  const bytecode = output.contracts[':PolicyContract'].bytecode;
  const abi = JSON.parse(output.contracts[':PolicyContract'].interface);
  const contract = new ethLib.contract(abi);

  return { contract, abi, bytecode }
}

async function createPolicyContract() {
  // Create a policy contract. Steps:
  // compile contract
  // identify its constructor
  // deploy contract (get address)
  try {
    let { contract, abi, bytecode } = await parseContract();
    
    await ethLib.unlockAccount(await ethLib.coinbase(), "");
    let deployFunc = ethLib.toPromise(contract.new)
    let deployed = await contract.deploy({data: '0x' + bytecode, arguments: [1111] })
                          .send({from: await ethLib.coinbase(), gas: 900000*2, gasPrice: '30000000000000' });
    let contractAddress = deployed.options.address;

    await initSubscriptions(contractAddress);
    await testDeployedContract(contractAddress);

  } catch (err) {
    console.log(err);
  }
}

async function initSubscriptions(contractAddress) {
  let { abi } = await parseContract();
  let contract = new ethLib.contract(abi, contractAddress, { from: await ethLib.coinbase(), gasPrice: '20000000000' });

  contract.events.requestCheckAccess({}, (err, event) => {
    // console.log(event);

  })
}

async function testDeployedContract(contractAddress) {
  let { abi } = await parseContract();
  let contract = new ethLib.contract(abi, contractAddress, { from: await ethLib.coinbase(), gasPrice: '20000000000' });
  // console.log(1111 === Number(await contract.methods.getDevId().call({ from: await ethLib.coinbase()})));
}

async function test() {
  // console.log(await ethLib.getAccounts());
}

let app = express();

app.get('/', (req, res) => {
  res.send('I am end device');
});

app.get('/start-connect', (req, res) => {

});

var server = app.listen(8080, () => {
  // uncomment to deploy contract

  // test();
  // web3.eth.net.isListening().then(() => console.log("is Listening \n"))
  // createPolicyContract();

  console.log(`App listening on ${server.address().port}`);
});