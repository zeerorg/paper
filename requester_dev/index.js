var Web3 = require('web3');
var express = require('express');
var fs = require('fs');
var solc = require('solc');
var MyEthClass = new require('ethlib').Class;

// TODO: deploy authorization list contract

var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
var ethLib = new MyEthClass(web3);

let contractOutputCache = null;

async function parseAuthListContract() {
  let output;
  if (!contractOutputCache) {
    const input = fs.readFileSync('../contract/auth_list.sol');
    output = solc.compile(input.toString(), 1);
    contractOutputCache = output;
  } else {
    output = contractOutputCache;
  }
  // console.log(output.contracts)
  const bytecode = output.contracts[':AuthList'].bytecode;
  const abi = JSON.parse(output.contracts[':AuthList'].interface);
  const contract = new ethLib.contract(abi);

  return { contract, abi, bytecode }
}

async function deployContract() {
  let { contract, abi, bytecode } = await parseAuthListContract();

  await ethLib.unlockAccount(await ethLib.coinbase2(), "");
  let deployFunc = ethLib.toPromise(contract.new)
  let deployed = await contract.deploy({data: '0x' + bytecode, arguments: [] })
                        .send({from: await ethLib.coinbase2(), gas: 900000*2, gasPrice: '30000000000000' });
  let contractAddress = deployed.options.address;

  console.log(contractAddress);
}

// Create server ==========================

let app = express();

app.get('/', (req, res) => {
  res.send('I am requester');
});

var server = app.listen(8080, async () => {
  try {
    await deployContract();
  } catch(err) {
    console.log(err);
  }
  console.log(`App listening on ${server.address().port}`);
});