function toPromise(getStuff){
  return (...args) => {
    return new Promise((resolve,reject) => {
        // console.log(args);
        getStuff(...args ,function(err,data){
            if(err !== null) return reject(err);
            resolve(data);
        });
    });
  }
}

function MyEthClass (web3) {
  this.web3 = web3;
  this.getAccounts = toPromise(web3.eth.getAccounts)
  this.contract = web3.eth.Contract
  this.coinbase = toPromise(web3.eth.getCoinbase)
  this.coinbase2 = () => {
    return new Promise((resolve, reject) => {
      web3.eth.getAccounts().then(accs => resolve(accs[1]));
    });
  }
  this.toPromise = toPromise
  this.unlockAccount = web3.eth.personal.unlockAccount
}

module.exports.Class = MyEthClass;