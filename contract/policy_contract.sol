pragma solidity ^0.4.16;

contract AuthList {
    function addToken(bytes) public {}
    function removeToken(bytes) public {}
    function checkToken(bytes) public {}
}

contract PolicyContract {
    struct Token {
        bytes data;
    }

    address public owner;
    uint devId;  // ID for a resource (sensor) used to identify every policy contract differently

    constructor(uint _devId) public {
        owner = msg.sender;
        devId = _devId;
    }

    function getDevId() public view returns (uint) {
        return devId;
    }

    // ===== Properties used to check access ===== 
    event requestCheckAccess(bytes accessStructure, address requester);
    event accessChecked(bool granted, address requester);

    function checkAccess(bytes acStruct) public {
        emit requestCheckAccess(acStruct, msg.sender);
    }

    function accessCheckOutput(bool _granted, address _requester) public {
        emit accessChecked(_granted, _requester);
    }
    // ===== Finished properties ====


    // ===== Properties used to request access =====
    event requestAccessToken(bytes accessStructure, address requester, address authList);

    function requestAccess(bytes acStruct, address _authList) public {
        emit requestAccessToken(acStruct, msg.sender, _authList);
    }

    function createToken (address _authList, bytes _data) public {
        require(msg.sender == owner);
        AuthList authList = AuthList(_authList);
        authList.addToken(_data);
    }
    // ===== Finished Properties =====

    // ===== Revoke access =====
    function revokeAccess (address _authList, bytes _data) public {
        require(msg.sender == owner);
        AuthList authList = AuthList(_authList);
        authList.removeToken(_data);
    }
    // ===== Finished =====

    // ===== Verify token =====
    function verifyToken (address _authList, bytes _data) public {
        AuthList authList = AuthList(_authList);
        authList.checkToken(_data);
    }
}