pragma solidity ^0.4.16;

contract AuthList {

    address public owner;
    bytes[] public tokens;

    constructor() public {
        owner = msg.sender;
    }

    function addToken(bytes _token) public {
        tokens.push(_token);
        // How to verify signature
    }

    function removeToken(bytes _token) public {
        // removing token
    }

    function checkToken(bytes _token) public {
        // check if token is present
    }

}